package com.devcamp.j51_javabasic.s50;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class subtask2 {
    //create list number
    public static double[] createArray(){
    Random newRandom = new Random();
    double[] myDoubleArray = new double[100];

    for (int i = 0; i < myDoubleArray.length; i++ ){
        myDoubleArray[i] = Double.valueOf(newRandom.nextDouble());
    }
    return myDoubleArray;
}
    //convert double[] to Double[] 
    public static Double[] convertToDouble(double[] paramDoubleArr) {
    Double[] myDoubleArray = new Double[paramDoubleArr.length];
    
    for (int i = 0; i < paramDoubleArr.length; i++){
        myDoubleArray[i] = paramDoubleArr[i];
    }

    return myDoubleArray;
   
}
    //convert array to array list
    public static void convertArrToArrList(Double[] paramDoubleArr) {
    ArrayList<Double> myArrayList = new ArrayList<Double>();
    Collections.addAll(myArrayList, paramDoubleArr);
    System.out.println(myArrayList);

}

    public static void main(String[] args) {
    double[] doubleArray =  createArray();
    Double[] myDoubleArray = convertToDouble(doubleArray);
    subtask2.convertArrToArrList(myDoubleArray);
    }
}
