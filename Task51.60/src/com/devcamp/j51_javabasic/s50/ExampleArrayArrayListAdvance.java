package com.devcamp.j51_javabasic.s50;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

 public class ExampleArrayArrayListAdvance {
    //create list number
    public static long[] createArray(){
        Random newRandom = new Random();
        long[] myLongArray = new long[100];

        for (int i = 0; i < myLongArray.length; i++ ){
        myLongArray[i] = Long.valueOf(newRandom.nextLong());
       // System.out.println(myLongArray[i]);
    }

    return myLongArray;
    }
    //convert long[] to int[] and autoboxing to Integer[]
    public static Integer[] convertLongArrToInt(long[] paramLongArr) {
        int[] myIntArray = new int[paramLongArr.length];
        Integer[] myIntegerArray = new Integer[paramLongArr.length];
        for (int i=0; i< myIntArray.length; i++){
            myIntArray[i] = (int) paramLongArr[i];
        }
        
        for (int i = 0; i<myIntArray.length; i++){
            myIntegerArray[i] = myIntArray[i];
        }

        return myIntegerArray;
       
    }
    //convert array to array list
    public static void convertArrToArrList(Integer[] paramIntArr) {
        ArrayList<Integer> myArrayList = new ArrayList<Integer>();
        Collections.addAll(myArrayList, paramIntArr);
        System.out.println(myArrayList);

    }
    
   public static void main(String[] args) {
    long[] myLongArray =  createArray();
    Integer[] myIntegerArray = convertLongArrToInt(myLongArray);
    ExampleArrayArrayListAdvance.convertArrToArrList(myIntegerArray);
}
}
